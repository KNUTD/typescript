import {Triangle} from "./Triangle";

export class Circle {
    r: number;
    x0: number;
    y0: number;

    constructor(triangle) {
        this.r = Circle.radius(triangle);
        this.x0 = triangle.topX;
        this.y0 = triangle.topY - this.r + triangle.height;
    }

    static radius(triangle) {
        return triangle.base / 2 * Math.sqrt((2 * triangle.side - triangle.base) / (2 * triangle.side + triangle.base));
    }


    drawing(ctx) {
        ctx.beginPath();
        ctx.arc(this.x0, this.y0, this.r, 0, 2 * Math.PI, false);
        ctx.fillStyle = "#D3D3D3";
        ctx.fill();
        ctx.restore();
        ctx.closePath();
    }
}