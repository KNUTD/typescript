import {Rectangle} from "./Rect";

export class Triangle {
    leftX: number;
    leftY: number;
    rightX: number;
    rightY: number;
    topX: number;
    topY: number;
    height: number;
    base: number;
    side: number;

    constructor(rectangle) {
        this.leftX = rectangle.left;
        this.leftY = rectangle.top + rectangle.height;
        this.rightX = rectangle.right;
        this.rightY = rectangle.bottom;
        this.topX = rectangle.left + (rectangle.width / 2);
        this.topY = rectangle.top;
        this.height = rectangle.height;
        this.base = rectangle.width;
        this.side = Math.sqrt(this.height * this.height + (this.base / 2) * (this.base / 2));
    }

    area() {
        return (this.base * this.height) / 2;
    }

    drawing(ctx) {
        ctx.strokeStyle = "red";
        ctx.moveTo(this.leftX, this.leftY);
        ctx.lineTo(this.rightX, this.rightY);
        ctx.lineTo(this.topX, this.topY);
        ctx.lineTo(this.leftX, this.leftY);
        ctx.stroke();
        ctx.save();
    };
}