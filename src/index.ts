import {Rectangle} from "./Rect";
import {Circle} from "./Circle";
import {Triangle} from "./Triangle";
import {Point} from "./Point";

const canvas = <HTMLCanvasElement> document.getElementById("canvas");
const ctx =  canvas.getContext("2d");
const rect = new Rectangle(Point.random(), Point.random());
const triangle = new Triangle(rect);
const circle = new Circle(triangle);
triangle.drawing(ctx);
rect.drawing(ctx);
circle.drawing(ctx);
