export class Point {
    x: number;
    y: number;

    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    static random(xFrom = 0, xTo  = 500, yFrom  = 0, yTo  = 500) {
        return new Point(Point.randomNumber(xFrom, xTo), Point.randomNumber(yFrom, yTo));
    }

    static randomNumber(min, max) {
        return Math.random() * (max - min) + min;
    }

    drawing(ctx) {
        ctx.fillStyle = "red";
        ctx.fillRect(this.x, this.y, 1, 1);
    }
}